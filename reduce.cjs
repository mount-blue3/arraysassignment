function reduce(items,cb,startingValue){
    if(!Array.isArray(items)){
        console.log("enter correct array");
    }
    if(typeof cb !== "function"){
        console.log("proper call back function is needed");
    }
    let acc;
    if(typeof startingValue === "undefined"){
        acc = undefined;
    }
    else{
        acc = startingValue;
    }
    for(let index=0;index<items.length;index++){
        if(acc!==undefined){
            acc = cb(acc,items[index],index,items);
        }
        else{
            acc = items[index];
        }
        
    }
    return acc;
}
module.exports = reduce;