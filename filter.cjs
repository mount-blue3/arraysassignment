function filter(items,cb){
    let resArray=[];
    if(!Array.isArray(items)){
        console.log("enter correct array");
    }
    if(typeof cb !== "function"){
        console.log("proper call back function is needed");
    }  
    for(let index=0;index<items.length;index++){
    if(cb(items[index],index,items)===true)
    {resArray.push(items[index])}
    }
     return resArray;

}
module.exports = filter;