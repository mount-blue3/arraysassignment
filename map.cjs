function map(items,cb){
    let resArray=[];
    if(!Array.isArray(items)||items.length===0||!items){
        console.log("enter correct array");
    }
    if(typeof cb !== "function"){
        console.log("proper call back function is needed");
    }
    for(let index=0;index<items.length;index++){

        resArray.push(cb(items[index],index,items));
    }
    return resArray;
}
module.exports = map;