function each(items,cb){
    if(!Array.isArray(items)){
        console.log("enter correct array");
    }
    if(typeof cb !== "function"){
        console.log("proper call back function is needed");
    }
    for(let index in items){
        cb(items[index]);
    }
}
module.exports = each;