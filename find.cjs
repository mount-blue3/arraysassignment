function find(items,cb){
    let count=0;
    if(!Array.isArray(items)){
        console.log("enter correct array");
    }
    if(typeof cb !== "function"){
        console.log("proper call back function is needed");
    }  
    for(let item of items){
        if(cb(item))
        {
            count++;
            return item;
        }
    }
    if(count===0){
        return undefined;
    }
}
module.exports = find;
