function flatten(items,depth=1){
    if(!Array.isArray(items)){
        console.log("enter correct array");
        return;
    }
    let outputArray = [];
    for(let index =0;index<items.length;index++){
        if(Array.isArray(items[index])&&depth>0){
            outputArray = outputArray.concat(flatten(items[index],depth-1));
        }
        else{
            if(items[index]!==undefined&&items[index]!==null)
                outputArray.push(items[index]);
        }
    }
    
    return outputArray;
}
module.exports = flatten;